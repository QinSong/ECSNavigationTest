using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Demo
{
    public struct Unit_BufferElementData : IBufferElementData
    {
        public float3 wayPoints;
    }
}

