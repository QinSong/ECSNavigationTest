using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Experimental.AI;

namespace Demo
{
    public struct Unit_Component : IComponentData
    {
        public float3 fromLocation;
        public float3 toLocation;
        public NavMeshLocation nml_FromLocation;
        public NavMeshLocation nml_ToLocation;
        public bool routed;
        public bool reached;
        public bool isUsingCachePath;

        public float3 wayPointsDirection;
        public float speed;
        public float minDistanceReached;
        public int currentBufferIndex;
    }
}

