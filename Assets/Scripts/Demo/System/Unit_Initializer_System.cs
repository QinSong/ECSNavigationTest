﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using static UnityEditor.PlayerSettings;

namespace Demo
{
    public class Unit_Initializer_System : SystemBase
    {
        BeginInitializationEntityCommandBufferSystem bi_ecbs;
        public float elapsedTime;
        protected override void OnCreate()
        {
            bi_ecbs = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
            elapsedTime = 0;
        }

        protected override void OnUpdate()
        {
            EntityCommandBuffer.ParallelWriter ecb = bi_ecbs.CreateCommandBuffer().AsParallelWriter();
            elapsedTime += Time.DeltaTime;

            if (elapsedTime > UnitManager.Instance.spawnEvery)
            {
                elapsedTime = 0;
                Entities
                    .WithBurst(synchronousCompilation: true)
                    .ForEach((Entity entity, int entityInQueryIndex, in Unit_Initializer_Component uit, in LocalToWorld ltw) =>
                    {

                        for (int i = 0; i < uit.xGridCount; i++)
                        {
                            for (int j = 0; j < uit.zGridCount; j++)
                            {
                                Entity m_entity = ecb.Instantiate(entityInQueryIndex, uit.unitPref);

                                float3 pos = new float3(ltw.Position.x + i * uit.xPadding, ltw.Position.y + uit.baseOffset, ltw.Position.z + j * uit.zPadding) + uit.currentPosition;
                                UnitManager.Log("pos: " + pos.ToString());

                                ecb.SetComponent(entityInQueryIndex, m_entity, new Translation { Value = pos });
                                ecb.AddComponent<Unit_Component>(entityInQueryIndex, m_entity);
                                ecb.AddBuffer<Unit_BufferElementData>(entityInQueryIndex, m_entity);

                                Unit_Component uc = new Unit_Component();
                                uc.fromLocation = pos;
                                uc.toLocation = pos + new float3(0, 0, uit.destinationDistanceZAxis);
                                uc.speed = (float)new Unity.Mathematics.Random(uit.seed + (uint)(i*j)).NextDouble(uit.minSpeed, uit.maxSpeed);
                                uc.currentBufferIndex = 0;
                                uc.minDistanceReached = uit.minDistanceReached;
                                ecb.SetComponent(entityInQueryIndex, m_entity, uc);
                            }
                        }

                        //ecb.DestroyEntity(entityInQueryIndex, entity);
                    }).ScheduleParallel();
            }
            bi_ecbs.AddJobHandleForProducer(Dependency);
        }
    }
}
