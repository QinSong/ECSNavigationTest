using System.Collections.Generic;
using System.Diagnostics;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine.Experimental.AI;

namespace Demo
{
    public class Unit_System : SystemBase
    {
        private float3 extents;
        private Dictionary<int, float3[]> allPaths;
        private List<Entity> routedEntities;
        private List<NativeArray<int>> statusOutputs;
        private List<NativeArray<float3>> results;
        private List<NavMeshQuery> queries;
        private NavMeshWorld navMeshWorld;
        private List<JobHandle> jobHandles;

        protected override void OnCreate()
        {
            extents = new float3(100, 100, 100);
            allPaths = new Dictionary<int, float3[]>();
            statusOutputs = new List<NativeArray<int>>();
            results = new List<NativeArray<float3>>();
            routedEntities = new List<Entity>();
            queries = new List<NavMeshQuery>();
            jobHandles = new List<JobHandle>();

            for (int n = 0; n <= 32; n++)
            {
                NativeArray<float3> result = new NativeArray<float3>(1024, Allocator.Persistent);
                NativeArray<int> statusOutput = new NativeArray<int>(3, Allocator.Persistent);
                statusOutputs.Add(statusOutput);
                results.Add(result);
            }

            navMeshWorld = NavMeshWorld.GetDefaultWorld();
        }

        protected override void OnUpdate()
        {
            float deltaTime = Time.DeltaTime;
            int i = 0;
            Entities
                .WithoutBurst()
                 .WithStructuralChanges()
                .WithNone<Unit_Routed>()
                .ForEach((Entity entity, ref Unit_Component uc, ref DynamicBuffer<Unit_BufferElementData> ub) =>
                {
                    if (i <= UnitManager.Instance.maxEntityRoutedPerFrame)
                    {
                        int fromKey = ((int)uc.fromLocation.x + (int)uc.fromLocation.y + (int)uc.fromLocation.z) * UnitManager.Instance.maxPathSize;
                        int toKey = ((int)uc.toLocation.x + (int)uc.toLocation.y + (int)uc.toLocation.z) * UnitManager.Instance.maxPathSize;
                        int key = fromKey + toKey;

                        //Cache Path
                        if (UnitManager.Instance.useCache && allPaths.ContainsKey(key) && !uc.routed)
                        {
                            if (allPaths.TryGetValue(key, out float3[] waypoints))
                            {
                                for (int j = 0; j < waypoints.Length; j++)
                                {
                                    ub.Add(new Unit_BufferElementData() { wayPoints = waypoints[j] });
                                }
                                uc.routed = true;
                                uc.isUsingCachePath = true;
                                UnitManager.Log("AddComponent Unit_Routed 1");
                                EntityManager.AddComponent<Unit_Routed>(entity);
                                return;
                            }
                        }
                        //Job
                        else if (uc.routed == false)
                        {
                            NavMeshQuery curNavMeshQuery = new NavMeshQuery(navMeshWorld, Allocator.Persistent, UnitManager.Instance.maxPathNodePoolSize);

                            SinglePathFindingJob singlePathFindingJob = new SinglePathFindingJob()
                            {
                                query = curNavMeshQuery,
                                nml_FromLocation = uc.nml_FromLocation,
                                nml_ToLocation = uc.nml_ToLocation,
                                fromLocation = uc.fromLocation,
                                toLocation = uc.toLocation,
                                extents = extents,
                                maxIteration = UnitManager.Instance.maxIteration,
                                ub = ub,
                                result = results[i],
                                statusOutput = statusOutputs[i],
                                maxPathSize = UnitManager.Instance.maxPathSize
                            };
                            JobHandle handle = singlePathFindingJob.Schedule();
                            queries.Add(curNavMeshQuery);
                            routedEntities.Add(entity);
                            jobHandles.Add(handle);
                            handle.Complete();
                        }
                        i++;
                    }
                    else
                    {
                        return;
                    }
                }).Run();

            UnitManager.Log("11111111111111");

            for (int j = 0; j < jobHandles.Count; j++)
            {
                UnitManager.Log("222222222222");
                UnitManager.Log($"statusOutputs[j][0] = {statusOutputs[j][0]}");

                if (statusOutputs[j][0] == 1)
                {
                    UnitManager.Log("33333333333");
                    if (UnitManager.Instance.useCache && !allPaths.ContainsKey(statusOutputs[j][1]))
                    {
                        float3[] waypoints = new float3[statusOutputs[j][2]];

                        for (int k = 0; k < statusOutputs[j][2]; k++)
                        {
                            waypoints[k] = results[j][k];
                        }
                        if (waypoints.Length > 0)
                        {
                            allPaths.Add(statusOutputs[j][1], waypoints);
                        }
                    }

                    Unit_Component uc = EntityManager.GetComponentData<Unit_Component>(routedEntities[j]);
                    uc.routed = true;
                    EntityManager.SetComponentData(routedEntities[j], uc);
                    UnitManager.Log("AddComponent Unit_Routed 2");
                    EntityManager.AddComponent<Unit_Routed>(routedEntities[j]);
                }

                queries[j].Dispose();
            }
            routedEntities.Clear();
            jobHandles.Clear();
            queries.Clear();

            UnitManager.Log($"Movement");
            //Movement
            Entities
                .WithAll<Unit_Routed>()
                .ForEach((ref Unit_Component uc, ref DynamicBuffer<Unit_BufferElementData> ub, ref Translation trans) =>
                {
                    UnitManager.Log($"000000 ub.Length:{ub.Length}");
                    if (ub.Length > 0 && uc.routed)
                    {
                        UnitManager.Log("11111");
                        uc.wayPointsDirection = math.normalize(ub[uc.currentBufferIndex].wayPoints - trans.Value);
                        trans.Value += uc.wayPointsDirection * uc.speed * deltaTime;

                        if (!uc.reached
                                && ub.Length > 0
                                && math.distance(trans.Value, ub[uc.currentBufferIndex].wayPoints) <= uc.minDistanceReached
                                && uc.currentBufferIndex < ub.Length - 1)
                        {
                            UnitManager.Log("22222");
                            uc.currentBufferIndex = uc.currentBufferIndex + 1;
                            if (uc.currentBufferIndex == ub.Length - 1)
                                uc.reached = true;
                        }
                        else if (uc.reached
                                && ub.Length > 0
                                && math.distance(trans.Value, ub[uc.currentBufferIndex].wayPoints) <= uc.minDistanceReached
                                && uc.currentBufferIndex > 0)
                        {
                            UnitManager.Log("33333");
                            uc.currentBufferIndex = uc.currentBufferIndex - 1;
                            if (uc.currentBufferIndex == 0)
                                uc.reached = false;
                        }
                    }
                }).ScheduleParallel();
        }

        protected override void OnDestroy()
        {
            if (UnitManager.Instance == null)
                return;

            for (int i = 0; i < UnitManager.Instance.maxEntityRoutedPerFrame; i++)
            {
                statusOutputs[i].Dispose();
                results[i].Dispose();
            }
        }
    }

    [BurstCompile]
    public struct SinglePathFindingJob : IJob
    {
        PathQueryStatus status;
        PathQueryStatus returningStatus;
        public NavMeshLocation nml_FromLocation;
        public NavMeshLocation nml_ToLocation;
        public NavMeshQuery query;
        public float3 fromLocation;
        public float3 toLocation;
        public float3 extents;
        public int maxIteration;
        public DynamicBuffer<Unit_BufferElementData> ub;
        public NativeArray<float3> result;
        public NativeArray<int> statusOutput;
        public int maxPathSize;


        public void Execute()
        {
            nml_FromLocation = query.MapLocation(fromLocation, extents, 0);
            nml_ToLocation = query.MapLocation(toLocation, extents, 0);

            if (query.IsValid(nml_FromLocation) && query.IsValid(nml_ToLocation))
            {
                status = query.BeginFindPath(nml_FromLocation, nml_ToLocation, -1);
                UnitManager.Log($"singlePathFindingJob status: {status.ToString()}");
                if (status == PathQueryStatus.InProgress)
                {
                    UnitManager.Log("PathQueryStatus.InProgress");
                    status = query.UpdateFindPath(maxIteration, out int iterationsPerformed);
                }
                if (status == PathQueryStatus.Success)
                {
                    UnitManager.Log("PathQueryStatus.Success");
                    status = query.EndFindPath(out int pathSize);

                    NativeArray<PolygonId> polys = new NativeArray<PolygonId>(pathSize, Allocator.Temp);
                    NativeArray<NavMeshLocation> res = new NativeArray<NavMeshLocation>(pathSize, Allocator.Temp);
                    NativeArray<StraightPathFlags> straightPathFlags = new NativeArray<StraightPathFlags>(maxPathSize, Allocator.Temp);
                    NativeArray<float> vertexSide = new NativeArray<float>(maxPathSize, Allocator.Temp);
                    int straightPathCount = 0;
                    query.GetPathResult(polys);

                    returningStatus = PathUtils.FindStraightPath(
                           query
                         , fromLocation
                         , toLocation
                         , polys
                         , pathSize
                         , ref res
                         , ref straightPathFlags
                         , ref vertexSide
                         , ref straightPathCount
                         , maxPathSize);
                    UnitManager.Log($"returningStatus = {returningStatus.ToString()}");
                    if (returningStatus == PathQueryStatus.Success)
                    {
                        //UnitManager.Log($"returningStatus = Success , straightPathCount: {straightPathCount}");
                        int fromKey = ((int)fromLocation.x + (int)fromLocation.y + (int)fromLocation.z) * maxPathSize;
                        int toKey = ((int)toLocation.x + (int)toLocation.y + (int)toLocation.z) * maxPathSize;
                        int key = fromKey + toKey;
                        statusOutput[0] = 1;
                        statusOutput[1] = key;
                        statusOutput[2] = straightPathCount;

                        UnitManager.Log($"fromKey {fromKey},toKey {toKey},key {key}, straightPathCount: {straightPathCount}");

                        for (int i = 0; i < straightPathCount; i++)
                        {
                            result[i] = (float3)res[i].position + new float3(0f, 0.75f, 0f);
                            ub.Add(new Unit_BufferElementData() { wayPoints = res[i].position });
                            UnitManager.Log("ub.Add");
                        }
                        polys.Dispose();
                        res.Dispose();
                        straightPathFlags.Dispose();
                        vertexSide.Dispose();
                    }
                }
            }
            else
            {
                if (!query.IsValid(nml_FromLocation))
                    UnitManager.Log("nml_FromLocation not Valid");
                else if (!query.IsValid(nml_ToLocation))
                    UnitManager.Log("nml_ToLocation not Valid");
                else
                    UnitManager.Log("????????");
            }
        }
    }

    public struct Unit_Routed : IComponentData { }
}

