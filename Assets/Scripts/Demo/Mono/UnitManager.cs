using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demo
{
    public class UnitManager : MonoBehaviour
    {
        public static UnitManager Instance;
        public int maxPathSize;
        public int maxEntityRoutedPerFrame;
        public int maxPathNodePoolSize;
        public int maxIteration;
        public bool useCache;
        public float spawnEvery;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public static void Log(string log)
        {
            Debug.Log(log);
        }
    }
}
