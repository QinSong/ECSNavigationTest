﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
namespace Demo
{
    public class SpawnTestSys : SystemBase
    {

        BeginSimulationEntityCommandBufferSystem ebs;

        protected override void OnCreate()
        {
            ebs = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }
        protected override void OnUpdate()
        {
            EntityCommandBuffer.ParallelWriter ecb = ebs.CreateCommandBuffer().AsParallelWriter();
            Entities.ForEach((Entity entity, int entityInQueryIndex, in SpawnTestComp stc) =>
            {
                bool isSpawn = false;
                if (isSpawn == false)
                {
                    ecb.Instantiate(entityInQueryIndex, stc.prefab);
                    isSpawn = true;
                }

                //ecb.DestroyEntity(entityInQueryIndex , entity);
            }).ScheduleParallel();
            ebs.AddJobHandleForProducer(Dependency);
        }
    }

    [GenerateAuthoringComponent]
    public struct SpawnTestComp : IComponentData
    {
        public Entity prefab;
    }
}

