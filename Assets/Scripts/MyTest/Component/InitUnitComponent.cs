using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Demo
{
    [GenerateAuthoringComponent]
    public struct InitUnitComponent : IComponentData
    {
        public float3 offset;
        public Entity unitPref;

        public float3 startLocation;
        public float3 endLocation;
        public float minReachDis;

        public int maxIteration;
        public int maxStraightPath;
        public float3 extents;
        public int pathNodePoolSize;

        public float speed;
    }
}
