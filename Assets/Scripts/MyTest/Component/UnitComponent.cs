using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Experimental.AI;

namespace Demo
{
    public struct UnitNavigationComponent : IComponentData
    {
        public bool isRouted;

        //·������
        public float3 fromLocation;
        public float3 toLocation;
        public NavMeshLocation nml_fromLocation;
        public NavMeshLocation nml_toLocation;
        public int maxIteration;
        public int maxStraightPath;
        public float3 extents;
        public int pathNodePoolSize;
    }

    public struct UnitBufferElement : IBufferElementData
    {
        public float3 wayPoint;
    }

    public struct UnitRoutedTag : IComponentData { }

    public struct UnitMovementComponent: IComponentData
    {
        public int curWayPointIndex;
        public float speed;
        public float3 moveDir;
        public bool isReached;
        public float minReachDis;
    }
}
