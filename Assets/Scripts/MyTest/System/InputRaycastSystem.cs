using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

namespace Demo
{
    [AlwaysUpdateSystem]
    public class InputRaycastSystem : SystemBase
    {
        private CollisionWorld collisionWorld;
        private BuildPhysicsWorld bpw;
        //private Camera maincam;

        //private BeginSimulationEntityCommandBufferSystem bi_ecbs;

        protected override void OnCreate()
        {
            base.OnCreate();

            //maincam = Camera.main;
            bpw = World.GetOrCreateSystem<BuildPhysicsWorld>();
            //bi_ecbs = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            if (Input.GetMouseButtonDown(0))
            {
                ClickInput();
            }

            //bi_ecbs.AddJobHandleForProducer(Dependency);
        }

        private void ClickInput()
        {
            collisionWorld = bpw.PhysicsWorld.CollisionWorld;
            UnityEngine.Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float3 startPos = ray.origin;
            float3 endPos = ray.origin + ray.direction * 1000;
            if (Raycast(startPos, endPos, out Unity.Physics.RaycastHit closestHit))
            {
                //EntityCommandBuffer.ParallelWriter ecb = bi_ecbs.CreateCommandBuffer().AsParallelWriter();
                Entities.
                    WithStructuralChanges().
                    WithoutBurst().
                    WithAll<UnitRoutedTag>().
                    ForEach((Entity entity,  ref UnitNavigationComponent nav , in Translation translation) =>
                    {
                        //ecb.RemoveComponent<UnitRoutedTag>(entityInQueryIndex, entity);
                        EntityManager.RemoveComponent<UnitRoutedTag>(entity);
                        nav.isRouted = false;
                        nav.fromLocation = translation.Value;
                        nav.toLocation = closestHit.Position;
                    }).Run();
            }
        }

        private bool Raycast(float3 startPos, float3 endPos, out Unity.Physics.RaycastHit closestHit)
        {
            RaycastInput input = new RaycastInput()
            {
                Start = startPos,
                End = endPos,
                Filter = CollisionFilter.Default
                //Filter = new CollisionFilter()
                //{
                //    BelongsTo = 1 << 0,
                //    CollidesWith = 1 << 1
                //}
            };
            return collisionWorld.CastRay(input, out closestHit);
        }
    }
}
