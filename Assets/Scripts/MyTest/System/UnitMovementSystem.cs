using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UIElements;

namespace Demo
{
    public class UnitMovementSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            float deltaTime = Time.DeltaTime;
            Entities
                .WithAll<UnitRoutedTag>()
                .ForEach((Entity entity , int entityInQueryIndex, ref UnitMovementComponent umc,ref DynamicBuffer<UnitBufferElement> ub,ref Translation translation /*ref LocalToWorld ltw*/) => { 
                    if (!umc.isReached )
                    {
                        umc.moveDir = math.normalize(ub[umc.curWayPointIndex].wayPoint - translation.Value);
                        translation.Value += deltaTime * umc.speed * umc.moveDir;
                        //umc.moveDir = math.normalize(ub[umc.curWayPointIndex].wayPoint - ltw.Position);
                        //float3 pos = ltw.Position;
                        //pos += deltaTime * umc.speed * umc.moveDir;
                        //quaternion tarRot = quaternion.LookRotation(umc.moveDir, ltw.Up);
                        //quaternion curRot = math.nlerp(ltw.Rotation, tarRot, 0.5f);
                        //ltw.Value = float4x4.TRS(pos, curRot, new float3(1,1,1));

                        if (umc.curWayPointIndex < ub.Length - 1)
                        {
                            float dis = math.distance(ub[umc.curWayPointIndex].wayPoint, translation.Value);
                            //float dis = math.distance(ub[umc.curWayPointIndex].wayPoint, ltw.Position);

                            if (dis <= umc.minReachDis)
                            {
                                umc.curWayPointIndex++;
                            }
                        }
                        else
                        {
                            float dis = math.distance(ub[umc.curWayPointIndex].wayPoint, translation.Value);
                            //float dis = math.distance(ub[umc.curWayPointIndex].wayPoint, ltw.Position);

                            if (dis <= umc.minReachDis)
                            {
                                umc.isReached = true;
                            }
                        }
                    }
                }).ScheduleParallel();
        }

    }
}
