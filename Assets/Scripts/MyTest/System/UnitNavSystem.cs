using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Experimental.AI;

namespace Demo
{
    public class UnitNavSystem : SystemBase
    {
        private List<NavMeshQuery> queries;
        private NavMeshWorld navWorld;
        private BeginSimulationEntityCommandBufferSystem bi_ecbs;
        private Dictionary<Entity, NavDataGroup> entityNavDatas = new Dictionary<Entity, NavDataGroup>();

        private const int MAX_QUERY_PER_FRAME = 300;
        private const int MAX_PATH_SIZE = 1024;
        private const int MAX_ITERATION = 1024;
        private const int MAX_PATH_NODE_POOL_SIZE = 1024;

        protected override void OnCreate()
        {
            base.OnCreate();
            bi_ecbs = EntityManager.World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();

            queries = new List<NavMeshQuery>();
            navWorld = NavMeshWorld.GetDefaultWorld();
        }

        protected override void OnUpdate()
        {
            EntityCommandBuffer.ParallelWriter ecb = bi_ecbs.CreateCommandBuffer().AsParallelWriter();

            Entities
                .WithNone<UnitRoutedTag>()
                .WithoutBurst()
                .ForEach((Entity entity, int entityInQueryIndex, ref UnitNavigationComponent uc, ref UnitMovementComponent umc, ref DynamicBuffer<UnitBufferElement> ub) =>
                {
                    if (uc.isRouted == false /*&& uc.isRouting == false*/)
                    {
                        umc.isReached = false;
                        umc.curWayPointIndex = 0;


                        NavMeshQuery query = new NavMeshQuery(navWorld, Allocator.Persistent, MAX_PATH_NODE_POOL_SIZE);
                        queries.Add(query);

                        FindPathJob findPathJob = new FindPathJob()
                        {
                            query = query,
                            fromLocation = uc.fromLocation,
                            toLocation = uc.toLocation,
                            nml_fromLocation = uc.nml_fromLocation,
                            nml_toLocation = uc.nml_toLocation,
                            extents = new float3(50, 50, 50),
                            maxPathSize = MAX_PATH_SIZE,
                            maxIteration = MAX_ITERATION,
                            pathBuffer = ub,
                            uc = uc,
                            entity = entity,
                            sortIndex = entityInQueryIndex,
                            ecb = ecb
                        };

                        JobHandle jobHandle = findPathJob.Schedule();
                        jobHandle.Complete();
                    }

                }).Run();

            for (int i = 0; i < queries.Count; i++)
            {
                queries[i].Dispose();
            }
            queries.Clear();

            bi_ecbs.AddJobHandleForProducer(Dependency);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            for (int i = 0; i < queries.Count; i++)
            {
                queries[i].Dispose();
            }
            queries.Clear();
        }
    }

    [BurstCompile]
    public struct FindPathJob : IJob
    {
        public NavMeshQuery query;
        public NavMeshLocation nml_fromLocation;
        public NavMeshLocation nml_toLocation;
        public float3 fromLocation;
        public float3 toLocation;
        public float3 extents;
        public int maxPathSize;
        public int maxIteration;
        //public NativeArray<int> statuses;
        //public NativeArray<NavMeshLocation> straightPath;

        [NativeDisableContainerSafetyRestriction]
        public DynamicBuffer<UnitBufferElement> pathBuffer;
        public UnitNavigationComponent uc;

        public EntityCommandBuffer.ParallelWriter ecb;
        public Entity entity;
        public int sortIndex;

        public void Execute()
        {
            nml_fromLocation = query.MapLocation(fromLocation, extents, 0);
            nml_toLocation = query.MapLocation(toLocation, extents, 0);
            if (!query.IsValid(nml_fromLocation) || !query.IsValid(nml_toLocation))
            {
                Debug.LogError("WTF");
                return;
            }

            PathQueryStatus status = query.BeginFindPath(nml_fromLocation, nml_toLocation);
            Debug.LogError($"BeginFindPath status = {status}");

            if (status == PathQueryStatus.InProgress)
            {
                status = query.UpdateFindPath(maxIteration, out int iterationCount);
                Debug.LogError($"UpdateFindPath status = {status}");
            }

            if (status == PathQueryStatus.Success)
            {
                PathQueryStatus endStatus = query.EndFindPath(out int pathSize);
                Debug.LogError($"EndFindPath endStatus = {endStatus}");
                if (endStatus == PathQueryStatus.Success)
                {
                    NativeArray<PolygonId> polygons = new NativeArray<PolygonId>(pathSize, Allocator.Temp);
                    query.GetPathResult(polygons);

                    NativeArray<NavMeshLocation> straightPath = new NativeArray<NavMeshLocation>(pathSize, Allocator.Temp);
                    NativeArray<StraightPathFlags> straightPathFlags = new NativeArray<StraightPathFlags>(maxPathSize, Allocator.Temp);
                    NativeArray<float> vertexSide = new NativeArray<float>(maxPathSize, Allocator.Temp);
                    int straightPathCount = 0;
                    PathQueryStatus resultStatus = PathUtils.FindStraightPath(
                            query,
                            fromLocation,
                            toLocation,
                            polygons,
                            pathSize,
                            ref straightPath,
                            ref straightPathFlags,
                            ref vertexSide,
                            ref straightPathCount,
                            maxPathSize
                    );

                    Debug.Log($"resultStatus: {resultStatus}");
                    if (resultStatus == PathQueryStatus.Success)
                    {
                        Debug.Log("3333333");

                        for (int i = 0; i < straightPathCount; i++)
                        {
                            UnitBufferElement unitBufferElement = new UnitBufferElement();
                            unitBufferElement.wayPoint = straightPath[i].position;
                            pathBuffer.Add(unitBufferElement);
                        }

                        uc.isRouted = true;
                        UnitManager.Log("AddComponent Unit_Routed 2");
                        ecb.AddComponent<UnitRoutedTag>(sortIndex, entity);
                    }
                    else
                    {
                        Debug.LogError($"pathfinding execute error pathState={resultStatus}");
                    }

                    polygons.Dispose();
                    straightPath.Dispose();
                    straightPathFlags.Dispose();
                    vertexSide.Dispose();
                }


            }
        }
    }

    public struct NavDataGroup
    {
        public NavMeshQuery query;
    }
}
