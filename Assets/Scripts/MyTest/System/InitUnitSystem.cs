using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Demo
{
    public class InitUnitSystem : SystemBase
    {
        private BeginInitializationEntityCommandBufferSystem bi_ecbs;
        protected override void OnCreate()
        {
            base.OnCreate();

            bi_ecbs = EntityManager.World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
        }
        protected override void OnUpdate()
        {
            EntityCommandBuffer.ParallelWriter ecb = bi_ecbs.CreateCommandBuffer().AsParallelWriter();

            Entities.ForEach((Entity entity, int entityInQueryIndex, in InitUnitComponent iuc, in LocalToWorld ltw) =>
            {
                Entity unitEntity = ecb.Instantiate(entityInQueryIndex, iuc.unitPref);
                float3 initPos = iuc.startLocation + iuc.offset;
                ecb.SetComponent(entityInQueryIndex, unitEntity, new Translation() { Value = initPos });
                UnitNavigationComponent unitComponent = new UnitNavigationComponent();
                //·������
                unitComponent.fromLocation = iuc.startLocation;
                unitComponent.toLocation = iuc.endLocation;
                unitComponent.isRouted = false;

                unitComponent.maxIteration = iuc.maxIteration;
                unitComponent.maxStraightPath = iuc.maxStraightPath;
                unitComponent.extents = iuc.extents;
                unitComponent.pathNodePoolSize = iuc.pathNodePoolSize;
                ecb.AddComponent(entityInQueryIndex, unitEntity, unitComponent);

                UnitMovementComponent movementComponent = new UnitMovementComponent();
                movementComponent.curWayPointIndex = 0;
                movementComponent.speed = iuc.speed;
                movementComponent.minReachDis = iuc.minReachDis;
                movementComponent.isReached = false;
                ecb.AddComponent(entityInQueryIndex, unitEntity, movementComponent);

                ecb.AddBuffer<UnitBufferElement>(entityInQueryIndex, unitEntity);

                ecb.DestroyEntity(entityInQueryIndex, entity);
            }).Schedule();
            bi_ecbs.AddJobHandleForProducer(Dependency);
        }
    }
}
